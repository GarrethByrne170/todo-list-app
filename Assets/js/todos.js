//Check off todos by clicking on them
$("ul").on("click","li",function(){
	$(this).toggleClass("completed");	
});

//Click X to delete todo
$("ul").on("click","span",function(event){
	$(this).parent().fadeOut(500,function(){
		$(this).remove();
	});
	event.stopPropagation();

});

$("input[type ='text'").keypress(function(event){
	if(event.which === 13){
		//grabbing new todo text from input
		var todoText = $(this).val();
		//create a new li and add to ul
		$("ul").append("<li><span><i class='fa fa-trash-o' aria-hidden='true'></i> </span>" + todoText + "</li>");
		$(this).val("");
	}
});

$(".fa-calendar").click(function(){
	$("input[type ='text'").fadeToggle();
});